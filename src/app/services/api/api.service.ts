import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, finalize, delay } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class ApiService {
  private apiUri = '/api/';

  constructor(
    private router: Router,
    private http: HttpClient
  ) {}

  getRequest(name: string, options?: any): Observable<any> {
    return this.http.get<any>(this.apiUri + name, options).pipe(
      catchError((error: HttpErrorResponse) => {
        return this.handleError(error);
      }),
      finalize(() => {
      }),
      delay(400)
    );
  }

  postRequest(name: string, data: any, options?: any): Observable<any> {
    return this.http.post<any>(this.apiUri + name, data, options).pipe(
      catchError((error: HttpErrorResponse) => {
        return this.handleError(error);
      })
    );
  }
  putRequest(name: string, data: any): Observable<any> {
    return this.http.put<any>(this.apiUri + name, data).pipe(
      catchError((error: HttpErrorResponse) => {
        return this.handleError(error);
      })
    );
  }

  deleteRequest(name: string): Observable<any> {
    return this.http.delete<any>(this.apiUri + name).pipe(
      catchError((error: HttpErrorResponse) => {
        return this.handleError(error);
      })
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status !== 404) {
    }

    return throwError(error.status + '');
  }
}
