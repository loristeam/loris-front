import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './modules/homepage/pages/homepage/homepage.component';
import { RecipePageComponent } from './modules/recipe/pages/recipe-page/recipe-page.component';
import { FindRecipePageComponent } from './modules/recipe/pages/find-recipe-page/find-recipe-page.component';
import {ContactPageComponent} from "./modules/other/pages/contact-page/contact-page.component";

export const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/homepage'
  },
  {
    path: 'homepage',
    component: HomepageComponent
  },
  {
    path: 'recipe/:id',
    component: RecipePageComponent
  },
  {
    path: 'find-recipe',
    component: FindRecipePageComponent
  },
  {
    path: 'contact',
    component: ContactPageComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class RouteModule { }
