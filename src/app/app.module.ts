import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomepageModule } from './modules/homepage/homepage.module';
import { RouteModule } from './route.module';
import { ComponentModule } from './component/component.module';
import { RecipeModule } from './modules/recipe/recipe.module';
import { ApiService } from './services/api/api.service';
import { HttpClientModule } from '@angular/common/http';
import {OtherModule} from "./modules/other/other.module";


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HomepageModule,
    RouteModule,
    ComponentModule,
    RecipeModule,
    HttpClientModule,
    OtherModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
