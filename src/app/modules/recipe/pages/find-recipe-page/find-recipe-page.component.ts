import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../services/api/api.service';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-find-recipe-page',
  templateUrl: './find-recipe-page.component.html',
  styleUrls: ['./find-recipe-page.component.scss']
})
export class FindRecipePageComponent implements OnInit {
  rest = [];
  productList: any;
  result: any;

  constructor(private api: ApiService,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.api.getRequest('product').subscribe(res => {
      console.log(res);
      this.productList = res;
    });
  }

  addElement(item) {
    let test = this.rest.find(x => x.id == item.id);
    console.log(test, item);
    test ? this.toastr.info('Element jest już na liście') : this.rest.push(item);
  }

  deleteElement(index) {
    this.rest.splice(index, 1);
    console.log(this.rest);
    this.toastr.success('Pomyślnie usunięto element');
  }

  find() {
    const vegetables = [];
    for (let i = 0; i < this.rest.length; i++) {
      console.log('bang');
      vegetables[i] = this.rest[i].id;
    }
    console.log(vegetables);
    this.api.postRequest('search', vegetables).subscribe( res => {
      this.result = [];
      this.result = res;
    });
  }
}
