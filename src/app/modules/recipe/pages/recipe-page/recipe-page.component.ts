import { Component, OnInit } from '@angular/core';
import {ApiService} from "../../../../services/api/api.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-recipe-page',
  templateUrl: './recipe-page.component.html',
  styleUrls: ['./recipe-page.component.scss']
})
export class RecipePageComponent implements OnInit {
  recipeId = '';
  pageContent: any;

  constructor(private api: ApiService,
              private activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.recipeId = this.activeRoute.snapshot.paramMap.get('id');
    this.api.getRequest('/recipe/' + this.recipeId).subscribe( res => {
      this.pageContent = res;
    });
  }

}
