import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecipePageComponent } from './pages/recipe-page/recipe-page.component';
import { FindRecipePageComponent } from './pages/find-recipe-page/find-recipe-page.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ToastrModule} from "ngx-toastr";
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [RecipePageComponent, FindRecipePageComponent],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right'
    }),
    RouterModule
  ],
  exports: [RecipePageComponent]
})
export class RecipeModule { }
