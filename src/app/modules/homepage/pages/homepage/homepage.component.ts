import { Component, OnInit } from '@angular/core';
import {ApiService} from "../../../../services/api/api.service";

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  newsContainer = [];

  constructor(private api: ApiService) { }

  ngOnInit() {
    this.api.getRequest('recipe/random/5').subscribe( res => {
      this.newsContainer = res;
    });
  }

}
