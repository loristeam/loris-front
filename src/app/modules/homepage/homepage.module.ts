import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { ComponentModule } from '../../component/component.module';
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [HomepageComponent],
  imports: [
    CommonModule,
    ComponentModule,
    RouterModule
  ]
})
export class HomepageModule { }
